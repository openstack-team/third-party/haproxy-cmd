This package contains a helper to send commands through the haproxy socket,
so it is possible to maintain servers part of a cluster by draining,
enabling and stopping servers part of a backend.

hapc
needs an admin socket to be able to use it. So it is needed to add this
to /etc/haproxy/haproxy.cfg:

global
        stats socket /var/lib/haproxy/admin.sock mode 600 level admin

Then hapc will connect to it.
